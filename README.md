# Templates for echoip.slatecave.net

This repository contains templates for use with an [echoip](https://github.com/mpolden/echoip) service.

To comply with the original licencing (Half of the original template is still there) these templates are [licensed unde a 3-clause BSD License](LICENSE).

## Why?

The original templates have (in my opinion) a horrible div layout, also I'm not serving something with google fonts and cloudflare hosted css on my domain.

## Looks awesome, how do I use it?

You are welcome to fork these templates and use them for your own service, but please modify them to make sure they point at your service instead of slatecave.net for the assets and links (go templates are pretty limited when it comes to custom logic, which makes it neccessary to hardcode some things).

## How to get the `v4.` and `v6.` subdomains working?

Make the `v4` subdomain only resolve to the IPv4 address of your server and the `v6` one only resolve to the IPv6 address.

Relevant configuration for the caddy webserver looks like this:
```
(security) {
	header {
		Strict-Transport-Security "max-age=31536000; includeSubDomains"
		X-Content-Type-Options "nosniff"
		# Remove slatecave.net or replace it with yur own asset source
		# The echoip.slatecave.net is neccessary to allow the "search" form to work when submitted from the v4 or v6 subdomain
		Content-Security-Policy "default-src 'none'; script-src 'none'; style-src 'self' slatecave.net; img-src 'self' slatecave.net; media-src 'self'; base-uri 'none'; form-action 'self' echoip.slatecave.net ; frame-ancestors 'none'"
                Referrer-Policy "strict-origin"

		# Best practice headers, some caching and a middlefinger to google
		permissions-policy "interest-cohort=()"
		CacheControl "max-age=3600, min-fresh=600, stale-if-error=86400, must-revalidate"
	}
}

host echoip.slatecave.net v4.echoip.slatecave.net v6.echoip.slatecave.net {
        import security # set some security and best practice headers here
        # import write_logfile # I'm have the logging logic in its own block
        reverse_proxy * localhost:13246 {
                header_up Host echoip.slatecave.net
        }
}
```

